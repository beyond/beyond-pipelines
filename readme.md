# Beyond Pipelines

This repository contains automated Beyond pipelines

## List of pipelines
### Metadata publication
The aim of this pipeline is to automated the description (i.e. metadata) of usefull datasets from a CSV file from beyond nextcloud to Beyond's GeoNetwork. 
This pipeline is launched by the Beyond Data Manager when he/she wants to publish or update dataset metadata. This dag is not periodic. It has 5 tasks:

1. Get the csv file from nextcloud
2. Parse csv file
3. Generate iso19115 xml file (using [geometa](https://github.com/eblondel/geometa))
4. Upload xml file to GeoNetwork (using [geonapi](https://github.com/eblondel/geonapi))
5. Check and verify GeoNetwork

## License
This code is provided under the [CeCILL-B](https://cecill.info/licences/Licence_CeCILL-B_V1-en.html) free software license agreement.