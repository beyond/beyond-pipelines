from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator

def dummy_operator_init_workflow_graph():
    return 'Hello world from first Airflow DAG!'

dag = DAG('metadata_publication', description='metadata publication',
          schedule_interval='0 12 * * *',
          start_date=datetime(2017, 3, 20), catchup=False)

get_csv_file_from_nextcloud = PythonOperator(task_id='get_csv_file_from_nextcloud', python_callable=dummy_operator_init_workflow_graph, dag=dag)
parse_csv = PythonOperator(task_id='parse_csv', python_callable=dummy_operator_init_workflow_graph, dag=dag)
geometa_init_xml = PythonOperator(task_id='geometa_init_xml', python_callable=dummy_operator_init_workflow_graph, dag=dag)
geonapi_upload_xml = PythonOperator(task_id='geonapi_upload_xml', python_callable=dummy_operator_init_workflow_graph, dag=dag)
geonetwork_check_mdsheets = PythonOperator(task_id='geonetwork_check_mdsheets', python_callable=dummy_operator_init_workflow_graph, dag=dag)


get_csv_file_from_nextcloud >> parse_csv >> geometa_init_xml >> geonapi_upload_xml >> geonetwork_check_mdsheets